<?php


namespace App\Http\services\missions;


use App\Http\Dtos\NewMissionQuery;
use App\Http\services\Service;
use App\Models\Mission;
use App\Models\Planet;
use App\Models\Rover;

/**
 * Class NewMissionService
 * @package App\Http\services\missions
 */
class NewMissionService extends Service
{
    /**
     * @var NewMissionQuery
     */
    private $new_mission_query;

    /**
     * NewMissionService constructor.
     * @param NewMissionQuery $new_mission_query
     */
    public function __construct(NewMissionQuery $new_mission_query)
    {
        $this->new_mission_query = $new_mission_query;
        parent::__construct();
    }

    /**
     * @throws \App\Exceptions\LaunchingPointNotAllowedException
     * @throws \App\Exceptions\PathNotAllowedException
     */
    protected function execute()
    {
        $rover = new Rover(
                        $this->new_mission_query->getInitialX(),
                        $this->new_mission_query->getInitialY(),
                        $this->new_mission_query->getPointingDirection(),
                        $this->new_mission_query->getPath(),
                        $this->new_mission_query->isFixedFront()
                );

        $mission = new Mission(
            $rover,
            new Planet()
        );

        $mission->launchMission();

        $mission->setXDestination($mission->getRover()->getGpsCoordinateX());
        $mission->setYDestination($mission->getRover()->getGpsCoordinateY());

        return $mission;

    }
}
