<?php


namespace App\Http\services;


use App\Exceptions\MethodNotImplementedException;
use http\Exception\BadMethodCallException;

abstract class Service
{
    protected $status;

    public function __construct() {
        $this->status = $this->execute();
    }

    protected function execute() {
        throw new MethodNotImplementedException("Method not implemented");
    }

    /**
     * @return void
     */
    public function getStatus()
    {
        return $this->status;
    }

}
