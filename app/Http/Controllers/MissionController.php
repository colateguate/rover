<?php


namespace App\Http\Controllers;

use App\Http\Dtos\NewMissionQuery;
use App\Http\services\missions\NewMissionService;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class MissionController
 * @package App\Http\Controllers
 */
class MissionController extends BaseController
{
    /**
     * MissionController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Request $request
     * @return string
     */
    public function newMission(Request $request)
    {
        $new_mission_service = new NewMissionService(new NewMissionQuery(
            $request->input('initial_x'),
            $request->input('initial_y'),
            $request->input('pointing_direction'),
            $request->input('rover_id'),
            $request->input('planet_id'),
            $request->input('path'),
            $request->input('fixed_front')
        ));


        $response = array(
                        "status"=>"SUCCESS",
                        "status_code" => 200,
                        "destination" => "(".$new_mission_service->getStatus()->getXDestination().",".$new_mission_service->getStatus()->getYDestination().")"
                    );

        return json_encode($response);
    }
}
