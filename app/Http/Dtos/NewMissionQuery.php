<?php


namespace App\Http\Dtos;


/**
 * Class NewMissionQuery
 * @package App\Http\Dtos
 */
class NewMissionQuery
{
    /**
     * @var int|null
     */
    private $initial_x;
    /**
     * @var int|null
     */
    private $initial_y;
    /**
     * @var string
     */
    private $pointing_direction;
    /**
     * @var int|null
     */
    private $rover_id;
    /**
     * @var int|null
     */
    private $planet_id;
    /**
     * @var string
     */
    private $path;

    /**
     * @var bool
     */
    private $fixed_front;

    /**
     * NewMissionQuery constructor.
     * @param int|null $initial_x
     * @param int|null $initial_y
     * @param string $pointing_direction
     * @param int|null $rover_id
     * @param int|null $planet_id
     * @param string $path
     * @param bool $fixed_front
     */
    public function __construct(?int $initial_x, ?int $initial_y, string $pointing_direction, ?int $rover_id, ?int $planet_id, string $path, bool $fixed_front = false)
    {
        $this->initial_x = $initial_x;
        $this->initial_y = $initial_y;
        $this->pointing_direction = $pointing_direction;
        $this->rover_id = $rover_id;
        $this->planet_id = $planet_id;
        $this->path = $path;
        $this->fixed_front = $fixed_front;
    }

    /**
     * @return int|null
     */
    public function getInitialX(): ?int
    {
        return $this->initial_x;
    }

    /**
     * @return int|null
     */
    public function getInitialY(): ?int
    {
        return $this->initial_y;
    }

    /**
     * @return string
     */
    public function getPointingDirection(): string
    {
        return $this->pointing_direction;
    }

    /**
     * @return int|null
     */
    public function getRoverId(): ?int
    {
        return $this->rover_id;
    }

    /**
     * @return int|null
     */
    public function getPlanetId(): ?int
    {
        return $this->planet_id;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return bool
     */
    public function isFixedFront(): bool
    {
        return $this->fixed_front;
    }
}
