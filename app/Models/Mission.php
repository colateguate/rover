<?php


namespace App\Models;


use App\Exceptions\BadCoordinateException;
use App\Exceptions\LaunchingPointNotAllowedException;
use App\Exceptions\MissionAbortedException;
use App\Exceptions\PathNotAllowedException;

/**
 * Class Mission
 * @package App\Models
 */
class Mission
{
    /**
     * @var Rover
     */
    private $rover;
    /**
     * @var Planet
     */
    private $planet;
    /**
     * @var
     */
    private $status;
    /**
     * @var
     */
    private $x_destination;
    /**
     * @var
     */
    private $y_destination;

    /**
     * Mission constructor.
     * @param int $initial_x
     * @param int $initial_y
     * @param Rover $rover
     * @param Planet $planet
     * @param string $starting_path
     * @throws PathNotAllowedException
     */
    public function __construct(Rover $rover, Planet $planet)
    {
        $this->rover = $rover;
        $this->planet = $planet;
    }

    /**
     * @return Rover
     */
    public function getRover(): Rover
    {
        return $this->rover;
    }

    /**
     * @param Rover $rover
     */
    public function setRover(Rover $rover): void
    {
        $this->rover = $rover;
    }

    /**
     * @return Planet
     */
    public function getPlanet(): Planet
    {
        return $this->planet;
    }

    /**
     * @param Planet $planet
     */
    public function setPlanet(Planet $planet): void
    {
        $this->planet = $planet;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getXDestination()
    {
        return $this->x_destination;
    }

    /**
     * @param mixed $x_destination
     */
    public function setXDestination($x_destination): void
    {
        $this->x_destination = $x_destination;
    }

    /**
     * @return mixed
     */
    public function getYDestination()
    {
        return $this->y_destination;
    }

    /**
     * @param mixed $y_destination
     */
    public function setYDestination($y_destination): void
    {
        $this->y_destination = $y_destination;
    }


    /**
     * @throws LaunchingPointNotAllowedException
     * @throws MissionAbortedException
     */
    public function launchMission()
    {
        $exploded_path = str_split($this->getRover()->getMissionPath());

        // Check launching point
        if($this->getPlanet()->checkCoordinate($this->getRover()->getGpsCoordinateX(), $this->getRover()->getGpsCoordinateY())){
            foreach($exploded_path as $index => $next_movement){
                $this->checkNextMoveAndLaunch($next_movement, $this->getRover()->getGpsCoordinateX(), $this->getRover()->getGpsCoordinateY());
            }
            $this->setXDestination($this->getRover()->getGpsCoordinateX());
            $this->setYDestination($this->getRover()->getGpsCoordinateY());
        }
        else{
            $err_message = "Starting point (".$this->getRover()->getGpsCoordinateX().",".$this->getRover()->getGpsCoordinateY()."). Out of range";
            throw new LaunchingPointNotAllowedException($err_message);
        }


    }

    /**
     * @param string $next_move
     * @param int $current_x
     * @param int $current_y
     */
    private function checkNextMoveAndLaunch(string $next_move, int $current_x, int $current_y)
    {
        // Set to false. Rover are expensive, we can't afford a "false" true...
        $accessible_next_move = false;
        $new_front = null;
        switch ($next_move){
            case 'F':
                switch ($this->getRover()->getPointingDirection()){
                    case "N":
                        $target_x = $current_x;
                        $target_y = $current_y - 1;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "N";
                        }
                        break;
                    case "S":
                        $target_x = $current_x;
                        $target_y = $current_y + 1;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "S";
                        }
                        break;
                    case "E":
                        $target_x = $current_x + 1;
                        $target_y = $current_y;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "E";
                        }
                        break;
                    case "W":
                        $target_x = $current_x - 1;
                        $target_y = $current_y;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "W";
                        }
                        break;
                }
                break;
            case 'L':
                switch ($this->getRover()->getPointingDirection()){
                    case "N":
                        $target_x = $current_x - 1;
                        $target_y = $current_y;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "W";
                        }
                        break;
                    case "S":
                        $target_x = $current_x + 1;
                        $target_y = $current_y;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "E";
                        }
                        break;
                    case "E":
                        $target_x = $current_x;
                        $target_y = $current_y - 1;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "N";
                        }
                        break;
                    case "W":
                        $target_x = $current_x;
                        $target_y = $current_y + 1;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "S";
                        }
                        break;
                }
                break;
            case 'R':
                switch ($this->getRover()->getPointingDirection()){
                    case "N":
                        $target_x = $current_x + 1;
                        $target_y = $current_y;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "E";
                        }
                        break;
                    case "S":
                        $target_x = $current_x - 1;
                        $target_y = $current_y;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "W";
                        }
                        break;
                    case "E":
                        $target_x = $current_x;
                        $target_y = $current_y + 1;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "S";
                        }
                        break;
                    case "W":
                        $target_x = $current_x;
                        $target_y = $current_y - 1;

                        if(false === $this->getRover()->isFixedFront()){
                            $new_front = "N";
                        }
                        break;
                }
                break;
            default:
                break;
        }

        if(null !== $new_front){
            $this->getRover()->setPointingDirection($new_front);
        }

        if($this->getPlanet()->checkCoordinate($target_x, $target_y)){
            $accessible_next_move = true;
        }

        if($accessible_next_move){
            $this->getRover()->setGpsCoordinateX($target_x);
            $this->getRover()->setGpsCoordinateY($target_y);
        }else{
            $abort_message = "** MISSION ABORTED ** Can't reach coordinates (".$target_x.",".$target_y."). Latest reached point is: (".$current_x.",".$current_y.")";
            throw new MissionAbortedException($abort_message);
        }

    }
}
