<?php


namespace App\Models;


/**
 * Class Planet
 * @package App\Models
 */
class Planet
{
    // DEFAULT PLANET NAME
    const PLANET_NAME = "Mars";

    // DEFAULT PLANET DIMENSIONS
    const NORTH_EDGE = 0;
    const SOUTH_EDGE = 200;
    const EAST_EDGE = 200;
    const WEST_EDGE = 0;

    /**
     * @var int
     */
    private $id;
    /**
     * @var string|null
     */
    private $name;
    /**
     * @var int|null
     */
    private $north_edge;
    /**
     * @var int|null
     */
    private $south_edge;
    /**
     * @var int|null
     */
    private $east_edge;
    /**
     * @var int|null
     */
    private $west_edge;

    /**
     * Planet constructor.
     * @param string|null $name
     * @param int|null $north_edge
     * @param int|null $south_edge
     * @param int|null $east_edge
     * @param int|null $west_edge
     */
    public function __construct(?string $name = null, ?int $north_edge = null, ?int $south_edge = null, ?int $east_edge = null, ?int $west_edge = null)
    {
        $this->name = ($name) ? $name : self::PLANET_NAME;
        $this->north_edge = ($north_edge) ? $north_edge : self::NORTH_EDGE;
        $this->south_edge = ($south_edge) ? $south_edge : self::SOUTH_EDGE;
        $this->east_edge = ($east_edge) ? $east_edge : self::EAST_EDGE;
        $this->west_edge = ($west_edge) ? $west_edge : self::WEST_EDGE;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getNorthEdge(): ?int
    {
        return $this->north_edge;
    }

    /**
     * @param int|null $north_edge
     */
    public function setNorthEdge(?int $north_edge): void
    {
        $this->north_edge = $north_edge;
    }

    /**
     * @return int|null
     */
    public function getSouthEdge(): ?int
    {
        return $this->south_edge;
    }

    /**
     * @param int|null $south_edge
     */
    public function setSouthEdge(?int $south_edge): void
    {
        $this->south_edge = $south_edge;
    }

    /**
     * @return int|null
     */
    public function getEastEdge(): ?int
    {
        return $this->east_edge;
    }

    /**
     * @param int|null $east_edge
     */
    public function setEastEdge(?int $east_edge): void
    {
        $this->east_edge = $east_edge;
    }

    /**
     * @return int|null
     */
    public function getWestEdge(): ?int
    {
        return $this->west_edge;
    }

    /**
     * @param int|null $west_edge
     */
    public function setWestEdge(?int $west_edge): void
    {
        $this->west_edge = $west_edge;
    }


    /**
     * @param int $x
     * @param int $y
     * @return bool
     */
    public function checkCoordinate(int $x, int $y): bool
    {
        if(!(($x >= $this->getWestEdge() && $x <= $this->getEastEdge()) && ($y >= $this->getNorthEdge() && $y <= $this->getSouthEdge()))){
            return false;
        }
        else{
            return true;
        }
    }
}
