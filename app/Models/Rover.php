<?php


namespace App\Models;


use App\Exceptions\PathNotAllowedException;

/**
 * Class Rover
 * @package App\Models
 */
class Rover
{
    // DEFAULT PLANET NAME
    const ROVER_NAME = "Rover Mars alpha";

    /**
     * @var string|null
     */
    private $name;
    /**
     * @var int
     */
    private $gps_coordinate_x;
    /**
     * @var int
     */
    private $gps_coordinate_y;
    /**
     * @var string;
     */
    private $pointing_direction;
    /**
     * @var string
     */
    private $mission_path;
    /**
     * @var bool
     */
    private $fixed_front;


    /**
     * Rover constructor.
     * @param int $initial_x
     * @param int $initial_y
     * @param string $pointing_direction
     * @param string $mission_path
     * @param bool $fixed_front
     * @param string|null $name
     * @throws PathNotAllowedException
     */
    public function __construct(
                            int $initial_x,
                            int $initial_y,
                            string $pointing_direction,
                            string $mission_path,
                            bool $fixed_front,
                            ?string $name = null)
    {
        $this->gps_coordinate_x = $initial_x;
        $this->gps_coordinate_y = $initial_y;
        $this->pointing_direction = strtoupper($pointing_direction);

        $allowed_directions = "/^[frlFRL]+$/"; // Allowed directions

        if(preg_match($allowed_directions, $mission_path)){
            $this->mission_path = strtoupper($mission_path);
        }
        else{
            throw new PathNotAllowedException("Path not allowed. Unrecognized path directions.");
        }

        $this->fixed_front = $fixed_front;

        $this->name = ($name) ? $name : self::ROVER_NAME;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getGpsCoordinateX(): int
    {
        return $this->gps_coordinate_x;
    }

    /**
     * @param int $gps_coordinate_x
     */
    public function setGpsCoordinateX(int $gps_coordinate_x): void
    {
        $this->gps_coordinate_x = $gps_coordinate_x;
    }

    /**
     * @return int
     */
    public function getGpsCoordinateY(): int
    {
        return $this->gps_coordinate_y;
    }

    /**
     * @param int $gps_coordinate_y
     */
    public function setGpsCoordinateY(int $gps_coordinate_y): void
    {
        $this->gps_coordinate_y = $gps_coordinate_y;
    }

    /**
     * @return string
     */
    public function getPointingDirection(): string
    {
        return $this->pointing_direction;
    }

    /**
     * @param string $pointing_direction
     */
    public function setPointingDirection(string $pointing_direction): void
    {
        $this->pointing_direction = strtoupper($pointing_direction);
    }

    /**
     * @return string
     */
    public function getMissionPath(): string
    {
        return $this->mission_path;
    }

    /**
     * @param string $mission_path
     */
    public function setMissionPath(string $mission_path): void
    {
        $allowed_directions = "/^[frlFRL]+$/"; // Allowed directions
        if(preg_match($allowed_directions, $mission_path)){
            $this->mission_path = strtoupper($mission_path);
        }
        else{
            throw new PathNotAllowedException("Path not allowed. Unrecognized path directions.");
        }
    }

    /**
     * @return bool
     */
    public function isFixedFront(): bool
    {
        return $this->fixed_front;
    }

    /**
     * @param bool $fixed_front
     */
    public function setFixedFront(bool $fixed_front): void
    {
        $this->fixed_front = $fixed_front;
    }
}
