<?php


namespace App\Exceptions;


/**
 * Class MissionAbortedException
 * @package App\Exceptions
 */
class MissionAbortedException extends \Exception implements \Throwable
{
    /**
     *
     */
    public function report()
    {

    }

    /**
     * @return false|string
     */
    public function render()
    {
        $error = array("status"=>"ERROR", "status_code"=>500, "error_message"=>$this->getMessage());

        return json_encode($error);
    }
}
