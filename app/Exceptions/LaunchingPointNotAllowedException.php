<?php


namespace App\Exceptions;


/**
 * Class LaunchingPointNotAllowedException
 * @package App\Exceptions
 */
class LaunchingPointNotAllowedException extends \Exception implements \Throwable
{
    /**
     *
     */
    public function report()
    {

    }

    /**
     * @return false|string
     */
    public function render()
    {
        $error = array("status"=>"ERROR", "status_code"=>400, "error_message"=>$this->getMessage());

        return json_encode($error);
    }
}
