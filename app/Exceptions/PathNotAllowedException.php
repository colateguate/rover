<?php


namespace App\Exceptions;


/**
 * Class PathNotAllowedException
 * @package App\Exceptions
 */
class PathNotAllowedException extends \Exception implements \Throwable
{
    /**
     *
     */
    public function report()
    {

    }

    /**
     * @return false|string
     */
    public function render()
    {
        $error = array("status"=>"ERROR", "status_code"=>400, "error_message"=>$this->getMessage());

        return json_encode($error);
    }
}
