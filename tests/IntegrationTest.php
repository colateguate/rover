<?php


/**
 * Class IntegrationTests
 */
class IntegrationTest extends TestCase
{

    /**
     *
     */
    public function testNewMissionOK()
    {
        $this->post(
                    '/new_mission',
                    [
                        "initial_x" => 100,
                        "initial_y" => 100,
                        "pointing_direction" => "N",
                        "path" => "L",
                        "fixed_front" => 1
                    ]

        );

        $this->assertEquals('{"status":"SUCCESS","status_code":200,"destination":"(99,100)"}', $this->response->getContent());
    }

    /**
     *
     */
    public function testNewMissionLaunchingPointNotAllowedExceptionResponse()
    {
        $this->post(
            '/new_mission',
            [
                "initial_x" => -100,
                "initial_y" => 1000,
                "pointing_direction" => "N",
                "path" => "L",
                "fixed_front" => 1
            ]

        );

        $this->assertEquals('{"status":"ERROR","status_code":400,"error_message":"Starting point (-100,1000). Out of range"}', $this->response->getContent());
    }

    /**
     *
     */
    public function testMissionAbortedExceptionResponse()
    {
        $this->post(
            '/new_mission',
            [
                "initial_x" => 0,
                "initial_y" => 0,
                "pointing_direction" => "N",
                "path" => "L",
                "fixed_front" => 1
            ]

        );

        $this->assertEquals('{"status":"ERROR","status_code":500,"error_message":"** MISSION ABORTED ** Can\'t reach coordinates (-1,0). Latest reached point is: (0,0)"}', $this->response->getContent());
    }

    /**
     *
     */
    public function testPathNotAllowedExceptionResponse()
    {
        $this->post(
            '/new_mission',
            [
                "initial_x" => 0,
                "initial_y" => 0,
                "pointing_direction" => "N",
                "path" => "QWERTY",
                "fixed_front" => 1
            ]

        );

        $this->assertEquals('{"status":"ERROR","status_code":400,"error_message":"Path not allowed. Unrecognized path directions."}', $this->response->getContent());
    }
}
