<?php


class PlanetTest extends TestCase
{
    public function testDefaultPlanet()
    {
        $planet = new \App\Models\Planet();

        $this->assertEquals(
            $planet->getNorthEdge(), 0
        );

        $this->assertEquals(
            $planet->getSouthEdge(), 200
        );

        $this->assertEquals(
            $planet->getWestEdge(), 0
        );

        $this->assertEquals(
            $planet->getEastEdge(), 200
        );

        $this->assertEquals(
            $planet->getName(), "Mars"
        );
    }
}
