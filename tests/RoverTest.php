<?php


/**
 * Class RoverTest
 */
class RoverTest extends TestCase
{
    /**
     *
     */
    public function testRoverAttributes()
    {
        $this->assertClassHasAttribute("name", \App\Models\Rover::class);
        $this->assertClassHasAttribute("gps_coordinate_x",\App\Models\Rover::class);
        $this->assertClassHasAttribute("gps_coordinate_y",\App\Models\Rover::class);
        $this->assertClassHasAttribute("pointing_direction",\App\Models\Rover::class);
        $this->assertClassHasAttribute("mission_path",\App\Models\Rover::class);
        $this->assertClassHasAttribute("fixed_front",\App\Models\Rover::class);
    }

    /**
     * @throws \App\Exceptions\PathNotAllowedException
     */
    public function testCreateRover()
    {
        $initial_x = rand(0,1000);
        $initial_y = rand(0,1000);
        $pointing_direction = "N";
        $mission_path = "FRRRF";
        $fixed_front = rand(0,1);

        $rover = new \App\Models\Rover($initial_x, $initial_y, $pointing_direction, $mission_path, $fixed_front);

        $this->assertEquals($rover->getName(), "Rover Mars alpha");
        $this->assertEquals($rover->getGpsCoordinateX(), $initial_x);
        $this->assertEquals($rover->getGpsCoordinateY(), $initial_y);
        $this->assertEquals($rover->getMissionPath(), $mission_path);
        $this->assertEquals($rover->isFixedFront(), $fixed_front);

        return $rover;
    }

    /**
     * @return \App\Models\Rover
     * @throws \App\Exceptions\PathNotAllowedException
     */
    public function testPathNotAllowedExceptionOnConstructor()
    {
        $initial_x = rand(0,1000);
        $initial_y = rand(0,1000);
        $pointing_direction = "N";
        $mission_path = "FRRRFT";
        $fixed_front = rand(0,1);


        $this->expectException(\App\Exceptions\PathNotAllowedException::class);
        $rover = new \App\Models\Rover($initial_x, $initial_y, $pointing_direction, $mission_path, $fixed_front);

        return $rover;
    }

    /**
     * @depends testCreateRover
     * @param $rover
     */
    public function testPathNotAllowedExceptionOnSetter(\App\Models\Rover $rover)
    {
        $this->expectException(\App\Exceptions\PathNotAllowedException::class);
        $rover->setMissionPath("RLFG");
    }
}
