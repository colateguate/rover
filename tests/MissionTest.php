<?php


/**
 * Class MissionTest
 */
class MissionTest extends TestCase
{
    private function createPlanet()
    {
        $planet = new \App\Models\Planet();
        return $planet;
    }

    /**
     * @param $mission_path
     * @return \App\Models\Rover
     * @throws \App\Exceptions\PathNotAllowedException
     */
    private function createRover($mission_path, $planet)
    {
        $allowed_frontside = array("N", "S", "E", "W", "n","s","e","w");

        $x_origin = rand($planet->getWestEdge()+10, $planet->getEastEdge()-10); // +-10 in order to prevent pathNotAllowedException and MissionAbortedException;
        $y_origin = rand($planet->getNorthEdge()+10, $planet->getSouthEdge()-10); // +-10 in order to prevent pathNotAllowedException and MissionAbortedException;
        $pointing_direction = $allowed_frontside[rand(0,7)];

        $rover = new \App\Models\Rover(
            $x_origin,
            $y_origin,
            $pointing_direction,
            $mission_path,
            1
        );

        return $rover;
    }
    /**
     *
     */
    public function testMissionAttributes()
    {
        $this->assertClassHasAttribute("rover", \App\Models\Mission::class);
        $this->assertClassHasAttribute("planet",\App\Models\Mission::class);
        $this->assertClassHasAttribute("status",\App\Models\Mission::class);
        $this->assertClassHasAttribute("x_destination",\App\Models\Mission::class);
        $this->assertClassHasAttribute("y_destination",\App\Models\Mission::class);

        $rover_mock = $this->createMock(\App\Models\Rover::class);
        $planet_mock = $this->createMock(\App\Models\Planet::class);

        $this->assertIsObject($rover_mock);
        $this->assertIsObject($planet_mock);
    }

    /**
     * @param \App\Models\Rover $rover
     * @throws \App\Exceptions\PathNotAllowedException
     */
    public function testCreateMission()
    {
        $allowed_path_directions = array("F", "L", "R", "f","l","r");
        $mission_path = $allowed_path_directions[rand(0,5)];

        $planet = $this->createPlanet();
        $rover = $this->createRover($mission_path, $planet);

        $mission = new \App\Models\Mission($rover, $planet);

        $this->assertIsObject($mission->getRover());
        $this->assertIsObject($mission->getPlanet());

        return $mission;
    }

    /**
     * @depends testCreateMission
     * @param \App\Models\Mission $mission
     */
    public function testLaunchMission(\App\Models\Mission $mission)
    {
        $mission->launchMission();

        $this->assertIsInt($mission->getXDestination());
        $this->assertIsInt($mission->getYDestination());
    }


    /**
     * @depends testCreateMission
     * @param \App\Models\Mission $mission
     */
    public function testLaunchingPointNotAllowedException(\App\Models\Mission $mission)
    {
        $this->expectException(\App\Exceptions\LaunchingPointNotAllowedException::class);

        $mission->getRover()->setGpsCoordinateX(-3);
        $mission->launchMission();
    }

    /**
     * @depends testCreateMission
     * @param \App\Models\Mission $mission
     */
    /*public function testMissionAbortedException(\App\Models\Mission $mission)
    {
        $mission->getRover()->setPointingDirection("N");
        $mission_path = "";
        for($i=0; $i<=$mission->getRover()->getGpsCoordinateX(); $i++){
            $mission_path .="L";
        }
        var_dump($mission_path);
        $mission->getRover()->setMissionPath($mission_path);

        $this->expectException(\App\Exceptions\MissionAbortedException::class);
        $mission->launchMission();
    }*/
}
